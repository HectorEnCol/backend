/* MIDDLEWARE FOR AUTHENTICATION */
'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');

//-- Secret key used for generate token --
var secret = 'clave_secreta_proceso_desarrollo_proyecto_mitaller_javascript';

//-- Method for authorization -- 
exports.ensureAuth = function(req, res, next){
    //-- Validate if exists headers --
    if(!req.headers.authorization){
        return res.status(403).send({ message: 'La petición no tiene cabecera de autenticación' });
    }

    //-- Cleaning the token deleting trash characters --
    var token = req.headers.authorization.replace(/['"]+/g, '');

    //-- Process for capture information from token --
    try{
        var payload = jwt.decode(token, secret);

        if(payload.exp <= moment().unix()){
            return res.status(401).send({ message: 'El token ha expirado' });
        }
    }catch(ex){
        return res.status(404).send({ message: 'El token no es válido' });
    }

    //-- Capturing information in an object that exists always on the system --
    req.user = payload;

    next();
}