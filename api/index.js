'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = 3800;

//-- Connect DataBase --
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/mitaller', { useMongoCliente: true })
        .then(() => {
            console.log("La conexión a la base de datos mitaller se ha realizado con éxito");

            //-- Create Server --
            app.listen(port, () => {
                console.log("Servidor corriendo en http://localhost:3800");
            });
        })
        .catch(err => console.log(err));