/* SERVICE FOR CREATE A TOKEN FOR AUTHENTICATION */
'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');

//-- Secret key used for generate token --
var secret = 'clave_secreta_proceso_desarrollo_proyecto_mitaller_javascript';

//-- Method for generate token -- 
exports.createToken = function(user){
    //-- Information to include in token --
    var payload = {
        sub: user._id,
        email: user.email,
        sellerCode: user.sellerCode,
        typeUser: user.typeUser,
        iat: moment().unix(),
        exp: moment().add(30, 'days').unix
    };

    //-- Return of token using information and secret key --
    return jwt.encode(payload, secret);
};