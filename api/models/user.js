/* CREATION OF MODEL USER WITH ITS ATRIBUTES */
'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//-- Atributes --
var UserSchema = Schema({
    email: String,
    password: String,
    sellerCode: String,
    userType: String,
    date: String
});

//-- Export model --
module.exports = mongoose.model('User', UserSchema);