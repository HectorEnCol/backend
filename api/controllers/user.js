/* CONTROLLER FOR USERS */
'use strict'

//-- Define Libraries --
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');

//-- Define Model --
var User = require('../models/user');
const { param, use } = require('../routes/user');

//-- Define Services --
var jwt = require('../services/jwt');

//-- METHODS --
//-- Hello World --
function home(req, res){
    res.status(200).send({
        message: 'Hola Mundo desde el servidor de NodeJS'
    });
}

//-- pruebas --
function pruebas(req, res){
    res.status(200).send({
        message: 'Acción de pruebas en el servidor de NodeJS'
    });
}

//-- METHOD FOR CREATE A NEW USER --
function createUser(req, res){
    var params = req.body;
    var user = new User();

    //-- Validating the parameters inserted by the user and adding the info to the Model --
    if(params.email && params.password){
        user.email = params.email;

        if(params.userType)
            user.userType = params.userType;
        else
            user.userType = "administrador";

        if(params.sellerCode)
            user.sellerCode = params.sellerCode;
        else
            user.sellerCode = null;

        //-- Asign time to create user --
        user.date = moment().unix();

        //-- Controlling duplicate users --
        User.find({ email: user.email.toLowerCase() }).exec((err, users) => {
            if(err) return res.status(500).send({ message: 'Error en la petición de usuarios' });

            if(users && users.length >= 1){
                return res.status(200).send({ message: 'El usuario que intentas registrar ya existe en el sistema' });
            }else{
                //-- Encrypting the password --
                bcrypt.hash(params.password, null, null, (err, hash) => {
                    user.password = hash;

                    //-- Saving the user on the DB --
                    user.save((err, userStored) => {
                        //-- Case with an error --
                        if(err) return res.status(500).send({ message: 'Error al crear el usuario' });

                        //-- User created --
                        if(userStored){
                            res.status(200).send({ user: userStored });
                        }else{
                            res.status(404).send({ message: 'No se ha podido crear el usuario' });
                        }
                    });
                });
            }
        });

    }else{
        //-- Result for incomplete fields in the form --
        res.status(200).send({
            message: 'Envía todos los campos necesarios!!!'
        });
    }
}

//-- METHOD FOR GET INFORMATION ABOUT AN USER --
function getUser(req, res){
    var userId= req.params.id;

    //-- Search an user into data base by its id and return --
    User.findById(userId, (err, user) => {
        if(err) return res.status(500).send({ message: 'Error en la petición' });

        if(!user) return res.status(404).send({ message: 'El usuario no existe' });

        return res.status(200).send({ user }); 
    });
}

//-- METHOD FOR LOGIN --
function loginUser(req, res){
    var params = req.body;
    var email = params.email;
    var password = params.password;

    //-- Search an user into the database by email --
    User.findOne({ email: email }, (err, user) => {
        if(err) return res.status(500).send({ message: 'Error en la petición' });

        //-- User exists --
        if(user){
            //-- Compare the password --
            bcrypt.compare(password, user.password, (err, check) => {
                //-- Both passwords are equals --
                if(check){

                    if(params.gettoken){
                        //-- Generate and return token --
                        return res.status(200).send({
                            token: jwt.createToken(user)
                        });
                    }else{
                        //-- Return user information --
                        user.password = undefined;
                        return res.status(200).send({ user });
                    }
                //-- Passwords are not equals --
                }else{
                    return res.status(404).send({ message: 'La contraseña ingresada no coincide' });
                }
            });
        //-- User doesn´t exist --
        }else{
            return res.status(404).send({ message: 'El usuario no existe!!!' });
        }
    });
}

//-- Export Methods --
module.exports = {
    home,
    pruebas,
    createUser,
    getUser,
    loginUser
}