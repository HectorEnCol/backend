/* ROUTES FOR USERS */
'use strict'

//-- Import libraries --
var express = require('express');

//-- Define Controller --
var UserController = require('../controllers/user');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');

//-- Define Routes --
api.get('/home', UserController.home);
api.get('/pruebas', md_auth.ensureAuth, UserController.pruebas);
api.post('/register', UserController.createUser);
api.post('/login', UserController.loginUser);
api.get('/user/:id', md_auth.ensureAuth, UserController.getUser);

//-- Export route --
module.exports = api;